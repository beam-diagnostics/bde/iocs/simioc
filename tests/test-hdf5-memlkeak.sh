#!/bin/bash

source ~/.config/bde/dev.env

caput SIM1:cam1:AcquireTime 0.05
caput SIM1:cam1:NumImages 1

caput -S SIM1:HDF1:FilePath "/tmp"
caput -S SIM1:HDF1:FileName "1test_hdf5"
caput SIM1:HDF1:AutoIncrement 1
caput -S SIM1:HDF1:FileTemplate "%s%s_%d.h5"
caput SIM1:HDF1:AutoSave 1
#caput SIM1:HDF1:NumCapture 1000
caput SIM1:HDF1:StorePerform 1
caput SIM1:HDF1:StoreAttr 1
# disable hdf5 saving plugin
# caput SIM1:HDF1:EnableCallbacks 0
# enable hdf5 saving plugin
caput SIM1:HDF1:EnableCallbacks 1



caput -S SIM1:HDF2:FilePath "/tmp"
caput -S SIM1:HDF2:FileName "2test_hdf5"
caput SIM1:HDF2:AutoIncrement 1
caput -S SIM1:HDF2:FileTemplate "%s%s_%d.h5"
caput SIM1:HDF2:AutoSave 1
#caput SIM1:HDF2:NumCapture 1000
caput SIM1:HDF2:StorePerform 1
caput SIM1:HDF2:StoreAttr 1
# disable hdf5 saving plugin
# caput SIM1:HDF2:EnableCallbacks 0
# enable hdf5 saving plugin
caput SIM1:HDF2:EnableCallbacks 1


# disable any ongoing acquisition for now
caput SIM1:cam1:Acquire 0 >/dev/null 2>&1

ioc=$(pidof iocApp)
iter=10000
run=0
rm -f /tmp/*.h5


# start the acquisition
caput SIM1:cam1:Acquire 1 >/dev/null 2>&1

echo "USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND"
while [ $run -lt $iter ]; do
  # echo "++++++ ++++++ + ++ +++ + + run $run ++++++ ++++++ + ++ +++ + + "
  # show the memory use (look at VSZ/RSS column, in kB)
  ps -u -q $ioc | tail -n1
  # wait until ~ 1000 images have been generated and saved to hdf5 files
  sleep 50
  # show the memory use (look at VSZ column, in kB)
  # ps -u -q $ioc | tail -n1
  # get rid of the files..
  rm -f /tmp/*.h5
  run=$((run+1))
done

# stop the acquisition
caput SIM1:cam1:Acquire 0 >/dev/null 2>&1
sleep 1

rm -f /tmp/*.h5

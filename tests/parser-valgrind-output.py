#!/usr/bin/python
# https://stackoverflow.com/questions/34325305/filtering-out-junk-from-valgrind-output
#
import fileinput
import re
import sys

# skip records that contain one of these words
skip = {
    'epicsEventCreate',
    'registerInterruptUser',
    'epicsThreadCreate',
    'epicsEnvSet',
    'doInitRecord1',
    'createRingBuffer',
    'dbmfMalloc',
    'rsrv_init',
    'iocshInit'
}

START = re.compile("in loss record")
STOP = re.compile("^==\d+== $")
GOOD = re.compile(r"\.c[pp]?:\d+", re.M)


def main(in_file):
    in_line = False
    current = []
    records = []

    fp = open(in_file, 'r')
    lines = fp.readlines()
    fp.close()
    for line in lines:
        # print('line: %s' % line)
        if in_line:
            in_line = not STOP.search(line)
        else:
            in_line = START.search(line)

        if in_line:
            # print('start line: %s' % line)
            # cleanup the line of PID and loss record count
            line = re.sub('^==\d+== ', '', line)
            line = re.sub('in loss record.*', '', line)
            current.append(line)
        else:
            if len(current):
                # print('record:\n' + "".join(current))
                records.append("".join(current))
                current = []

    # print('nr records %d' % len(records))
    good_records = []
    for record in records:
        good = True
        for s in skip:
            if record.find(s) != -1:
                good = False
        if good:
            good_records.append(record)
            # print('GOOD record:\n' + "".join(record))

    print('nr records %d' % len(records))
    print('nr GOOD records %d' % len(good_records))
    out_file = in_file + '-all'
    with open(out_file, 'w') as fp:
        fp.write(''.join(records))
    out_file = in_file + '-good'
    with open(out_file, 'w') as fp:
        fp.write('\n'.join(good_records))


if __name__ == "__main__":
    main(sys.argv[1])

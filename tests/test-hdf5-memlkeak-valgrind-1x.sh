#!/bin/bash

source ~/.config/bde/dev.env

rm -f /tmp/*.h5

NUM=1

caput SIM1:cam1:NumImages $NUM
caput SIM1:cam1:AcquireTime 0.05
caput SIM1:cam1:ImageMode Multiple
caput -S SIM1:HDF1:FilePath "/tmp"
caput -S SIM1:HDF1:FileName "test_hdf5"
caput SIM1:HDF1:AutoIncrement 1
caput -S SIM1:HDF1:FileTemplate "%s%s_%d.h5"
caput SIM1:HDF1:AutoSave 1
caput SIM1:HDF1:NumCapture 1

# disable hdf5 saving plugin
caput SIM1:HDF1:EnableCallbacks 0
# enable hdf5 saving plugin
#caput SIM1:HDF1:EnableCallbacks 1

# disable any ongoing acquisition for now
caput SIM1:cam1:Acquire 0 >/dev/null 2>&1

sleep 1

caput SIM1:cam1:Acquire 1 >/dev/null 2>&1
# wait until 1 image has been generated and saved to hdf5 file
while [ 1 ]; do
  done=$(caget -t SIM1:cam1:Acquire)
  if [[ $done == Done ]]; then
    echo "DONE!"
    break
  fi
  sleep 1
done

# ioc=$(pidof valgrind.bin)
# iter=1
# run=0
# rm -f /tmp/*.h5
# echo "USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND"
# while [ $run -lt $iter ]; do
#   # echo "++++++ ++++++ + ++ +++ + + run $run ++++++ ++++++ + ++ +++ + + "
#   # start the acquisition
#   caput SIM1:cam1:Acquire 1 >/dev/null 2>&1
#   # wait until all 1000 images have been generated and saved to hdf5 files
#   sleep 5
#   # show the memory use (look at VSZ column, in kB)
#   ps -u -q $ioc | tail -n1
#   # get rid of the files..
#   rm -f /tmp/*.h5
#   run=$((run+1))
# done

rm -f /tmp/*.h5

# leak tests

## test leaking without HDF5

Setup acq, keep HDF5 plugin disabled, look at VSZ column from ps.

	$ bash test-hdf5-memlkeak.sh 
	Old : SIM1:cam1:AcquireTime          0.05
	New : SIM1:cam1:AcquireTime          0.05
	Old : SIM1:HDF1:FilePath /tmp
	New : SIM1:HDF1:FilePath /tmp
	Old : SIM1:HDF1:FileName test_hdf5
	New : SIM1:HDF1:FileName test_hdf5
	Old : SIM1:HDF1:AutoIncrement        Yes
	New : SIM1:HDF1:AutoIncrement        Yes
	Old : SIM1:HDF1:FileTemplate %s%s_%d.h5
	New : SIM1:HDF1:FileTemplate %s%s_%d.h5
	Old : SIM1:HDF1:AutoSave             Yes
	New : SIM1:HDF1:AutoSave             Yes
	Old : SIM1:HDF1:NumCapture           1000
	New : SIM1:HDF1:NumCapture           1000
	Old : SIM1:HDF1:EnableCallbacks      Disable
	New : SIM1:HDF1:EnableCallbacks      Disable
	USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
	hinxx     4132  5.0  1.9 4724084 320648 pts/6  Sl+  08:25   0:55 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  4.7  1.9 4724084 320648 pts/6  Sl+  08:25   0:56 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  4.5  1.9 4724084 320648 pts/6  Sl+  08:25   0:57 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  4.4  1.9 4724084 320648 pts/6  Sl+  08:25   0:58 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  4.2  1.9 4724084 320648 pts/6  Sl+  08:25   0:59 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  4.0  1.9 4724084 320648 pts/6  Sl+  08:25   0:59 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  3.9  1.9 4724084 320648 pts/6  Sl+  08:25   1:00 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  3.8  1.9 4724084 320648 pts/6  Sl+  08:25   1:01 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  3.7  1.9 4724084 320648 pts/6  Sl+  08:25   1:02 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  3.6  1.9 4724084 320648 pts/6  Sl+  08:25   1:03 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd



## test leaking with HDF5

Setup acq, keep HDF5 plugin enabled, look at VSZ column from ps.

	$ bash test-hdf5-memlkeak.sh 
	Old : SIM1:cam1:AcquireTime          0.05
	New : SIM1:cam1:AcquireTime          0.05
	Old : SIM1:HDF1:FilePath /tmp
	New : SIM1:HDF1:FilePath /tmp
	Old : SIM1:HDF1:FileName test_hdf5
	New : SIM1:HDF1:FileName test_hdf5
	Old : SIM1:HDF1:AutoIncrement        Yes
	New : SIM1:HDF1:AutoIncrement        Yes
	Old : SIM1:HDF1:FileTemplate %s%s_%d.h5
	New : SIM1:HDF1:FileTemplate %s%s_%d.h5
	Old : SIM1:HDF1:AutoSave             Yes
	New : SIM1:HDF1:AutoSave             Yes
	Old : SIM1:HDF1:NumCapture           1000
	New : SIM1:HDF1:NumCapture           1000
	Old : SIM1:HDF1:EnableCallbacks      Disable
	New : SIM1:HDF1:EnableCallbacks      Enable
	USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
	hinxx     4132  2.9  2.2 4724084 363944 pts/6  Sl+  08:25   1:16 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  3.2  2.4 4789620 407292 pts/6  Sl+  08:25   1:26 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  3.4  2.7 4855156 450636 pts/6  Sl+  08:25   1:34 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  3.7  3.0 4855156 494196 pts/6  Sl+  08:25   1:42 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  3.9  3.2 4920692 537548 pts/6  Sl+  08:25   1:50 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  4.0  3.5 4986228 580896 pts/6  Sl+  08:25   1:58 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  4.2  3.8 4986228 624192 pts/6  Sl+  08:25   2:05 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  4.4  4.0 5051764 667548 pts/6  Sl+  08:25   2:14 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  4.6  4.3 5117300 711156 pts/6  Sl+  08:25   2:24 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx     4132  4.8  4.6 5117300 754452 pts/6  Sl+  08:25   2:33 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd

Leak: 5117300 - 4724084 = 393 MB

## test with applied H5close() patch

See https://github.com/areaDetector/ADCore/pull/390

	hinxx@obzen ~/bde/R3.15.5/simioc-master/tests $ bash test-hdf5-memlkeak.sh 
	Old : SIM1:cam1:AcquireTime          0.05
	New : SIM1:cam1:AcquireTime          0.05
	Old : SIM1:HDF1:FilePath \003
	New : SIM1:HDF1:FilePath /tmp
	Old : SIM1:HDF1:FileName \003
	New : SIM1:HDF1:FileName test_hdf5
	Old : SIM1:HDF1:AutoIncrement        No
	New : SIM1:HDF1:AutoIncrement        Yes
	Old : SIM1:HDF1:FileTemplate \003
	New : SIM1:HDF1:FileTemplate %s%s_%d.h5
	Old : SIM1:HDF1:AutoSave             No
	New : SIM1:HDF1:AutoSave             Yes
	Old : SIM1:HDF1:NumCapture           1
	New : SIM1:HDF1:NumCapture           1000
	Old : SIM1:HDF1:EnableCallbacks      Disable
	New : SIM1:HDF1:EnableCallbacks      Enable
	USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
	hinxx    30325 15.1  0.3 4459868 57460 pts/6   Sl+  11:58   0:11 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx    30325 15.6  0.3 4459868 60628 pts/6   Sl+  11:58   0:22 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx    30325 15.6  0.3 4459868 63796 pts/6   Sl+  11:58   0:33 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx    30325 15.6  0.4 4459868 66964 pts/6   Sl+  11:58   0:44 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx    30325 15.7  0.4 4459868 70136 pts/6   Sl+  11:58   0:55 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx    30325 15.6  0.4 4459868 73304 pts/6   Sl+  11:58   1:06 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx    30325 15.6  0.4 4459868 76472 pts/6   Sl+  11:58   1:17 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx    30325 15.6  0.4 4459868 79640 pts/6   Sl+  11:58   1:28 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx    30325 15.6  0.5 4459868 82548 pts/6   Sl+  11:58   1:39 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd
	hinxx    30325 15.6  0.5 4459868 85716 pts/6   Sl+  11:58   1:50 /opt/bde/R3.15.5/artifacts/simapp-master/bin/linux-x86_64/iocApp st.cmd

No leak.

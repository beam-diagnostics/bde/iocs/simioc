< envPaths

errlogInit(20000)

dbLoadDatabase("$(SIMAPP)/dbd/iocApp.dbd")
iocApp_registerRecordDeviceDriver(pdbbase)

epicsEnvSet("PREFIX", "SIM1:")
epicsEnvSet("PORT",   "SIM1")
epicsEnvSet("QSIZE",  "20")
epicsEnvSet("XSIZE",  "300")
epicsEnvSet("YSIZE",  "300")
epicsEnvSet("NCHANS", "2048")
epicsEnvSet("CBUFFS", "500")
epicsEnvSet("MAX_THREADS", "8")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(TOP)/db:$(AUTOSAVE)/db:$(ADCORE)/db:$(ADSIMDETECTOR)/db:$(SIMAPP)/db")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")
epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST", "NO")
epicsEnvSet("EPICS_CA_ADDR_LIST", "localhost")


# Create a simDetector driver
# simDetectorConfig(const char *portName, int maxSizeX, int maxSizeY, int dataType,
#                   int maxBuffers, int maxMemory, int priority, int stackSize)
simDetectorConfig("$(PORT)", $(XSIZE), $(YSIZE), 1, 0, 0)
dbLoadRecords("simDetector.template","P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")
# Create a standard arrays plugin, set it to get data from first simDetector driver.
NDStdArraysConfigure("Image1", 20, 0, "$(PORT)", 0, 0, 0, 0, 0, 5)
# This waveform only allows transporting 16-bit images
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Int16,FTVL=SHORT,NELEMENTS=1200000")

set_requestfile_path("./")
set_requestfile_path("$(TOP)/req")
set_requestfile_path("$(SIMAPP)/req")
set_requestfile_path("$(ADSIMDETECTOR)/req")
set_requestfile_path("$(ADCORE)/req")
set_requestfile_path("$(CALC)/req")
set_savefile_path("$(TOP)/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")
save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("save_restoreStatus.db", "P=$(PREFIX)")

# Load all other plugins using commonPlugins.cmd
< commonPlugins.cmd

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

iocInit()

# save things every thirty seconds
create_monitor_set("auto_settings.req", 30, "P=$(PREFIX)")
